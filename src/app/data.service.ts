import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export class Data {
  id: number;
  name:string;
  age:number;
  email:string;
}


@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _url : string = "/assets/data.json";

  constructor(private _http:HttpClient) { }

  getData():Observable<Data[]> {
    return this._http.get<Data[]>(this._url);
  }
}
