import { Component, OnInit } from '@angular/core';
import {DataService} from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'age', 'email'];


public data = []; 
  constructor( private dataService:DataService) {}
ngOnInit() {
  this.dataService.getData().subscribe(data => this.data = data);
}

}
 